# Ubuntu image to test MPTCP in GNS-3

## Description

This docker image is based on [gns3/ipterm](https://hub.docker.com/r/gns3/ipterm) docker image.

### Additional packages added
The packages added to the base image are visible in the ```Dockerfile```.

## Build locally to test 

To test if the image can be build without error, test locally on your machine with the command:
```
docker build -t ubuntu-mptcp .
```

## Force rebuild

If we change the content of the ```Dockerfile``` we have to add a new Tag to this repo. Then the CI/CD Pipeline will be executed.
Go to the "Repository--> Tag" menu to add the latest version number.

## How to use this image

To download and use this image, just type:
```
docker pull registry.forge.hefr.ch/bun/dockers/xxxxx
```

## More informations
See Gitlab Docs:
* https://docs.gitlab.com/ee/user/packages/container_registry/



<hr>
(c) F. Buntschu 28.04.2023