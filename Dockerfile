# Extend the ubuntu docker with some apps

FROM ubuntu:latest

# Used by tzdata
ENV DEBIAN_FRONTEND=noninteractive

RUN set -x \
    && apt -yqq update \
    # Install apt-add-repository command
    && apt-get install -y software-properties-common \
    && rm -rf /var/lib/apt/lists/* \
    && apt-add-repository -sy ppa:dreibh/ppa \
    && apt -y install --no-install-recommends isc-dhcp-client dnsutils\
    && apt -y install python3 build-essential libssl-dev libffi-dev \
    && apt -y install python3-pip python3-dev openssh-server netcat \
    && apt -y install nmap wget iproute2\
    && apt -y install netperfmeter iperf3 mptcpize\
    && rm -rf /var/lib/apt/lists/* 
CMD ["bash"]